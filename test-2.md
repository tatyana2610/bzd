# Контрольный тест №2
### Безопасность в чрезвычайных ситуациях.
**Единая государственнная система предупреждения и ликвидации чрезвычайных ситуаций**

### 1. Кто возглавляет Правительственную комиссию по предупреждению и ликвидации ЧС и обеспечению пожарной безопасности?
* Один из заместителей председателя провительства
* Председатель Правительства РФ
* **Министр МЧС России** [2]

### 2. К какому виду органов управления относитcя Праивтельственная комиссия по предупреждению и ликвидации чрезвычайных ситуаций и обеспечению пожарной безопасности?
* **Координационным**
* Координирующим
* Оперативного управления
* Повседневным
* Постоянно действующим

### 3. Сколько устанавливается режимов функционирования РСЧС?
* Один
* Четыре
* **Три**
* Два

### 4. РСЧС состоит из:
### 4.1 Из каких подсистем состоит РСЧС?
* **Территориальных и функциональных подсистем**
* Только ведомственных подсистем
* Территориальных и региональных подсистем
* Функциональной и территориальной подсистем
* Только территориальных подсистем

### 5. Какой орган управления является постоянно действующим на межрегиональном уровне РСЧС?
* Штаб ГОСЧ
* **РЦ ГОЧС**
* Комиссия по ЧС
* Межведомственная комиссия
* МЧС [1]

### 6. Сколько существует уровней реагирования на ЧС?
>* 1) Федеральный
>* 2) Межрегиональный
>* 3) Региональный
>* 4) Мунициапальный
>* 5) Объектовый

* Два
* Четыре
* Один
* **Пять**
* Три

### 7. Органами повседневного управления являются:
* Дежурно-диспетчерские службы, поисково-спасательные службы, ЦУКСы
* **Центры управления в кризисных ситуациях, дежурно-диспетчерские службы**

### 8. Какая администрация исполнительной власти контролирует создание материальных ресурсов РСЧС на муниципальном уровне?
* Президента РФ
* **Местного самоуправления**
* Организаций
* ФАО РФ
* Субъекта РФ

### 9. Предназначение сил постоянной готовности РСЧС?
* **Для оперативного реагирования**
* Для чрезвычайного реагирования
* Для экстренного реагирования
* Для быстрого реагирования

### 10. Какой орган исполнительной власти проверят готовность аварийно-спасательных формирований к реагированию на ЧС?
* Местного самоуправления
* Министерства, агенства, службы
* **МЧС**
* Начальники формирований
* Субъекта РФ

### 11. На объекте экономики постоянно действующим органом управления по защите от ЧС является:
### 11.1 Постоянно действующим органом управления на объекте экономики является:
* Администрация
* **Структурное подразделение Организаций**
* Управления ГОЧС
* КЧС и ПБ
* Руководитель объекта
* Не устанавливается
* Повседневной готовности

### 12. В каком режиме функционирования РСЧС может устанавливаться уровень реагирования на ЧС?
* При федеральной ЧС
* **Чрезвычайной ситуации**
* Не устанавливается
* Повседневной готовности

### 13. Существуют ли силы РСЧС постоянной готовности?
* Нет
* Только силы ликвидации ЧС
* **Да**

### 14. Органом повседневного управления РСЧС на федеральном уровне является
* РЦ ГОЧС
* КЧС
* **ЦУКС**
* МЧС
* ГУ ГОЧС

### 15. Постоянно действующим органом управления на объекте экономики является
* Отдел, сектор, штаб ГОЧС
* **Структура подразделения Организаций**
* КЧС
* Эвакуационная комиссия

### 16. Какой из перечисленных органов управления РСЧС осуществляет контроль за подготовкой населения в области защиты от ЧС
* ГУ МЧС
* ЕДДС
* ДДС
* КЧС
* **МЧС**

### 17. Система управления РСЧС - это:
* Повседневные, постоянно действующие, координирующие органы управления
* МЧС России, РЦ ГОЧС, ГУ ГОЧС
* **Органы управления, пункты управления, средства управления** 

### 18. На каких ПУ размещаются органы управления РСЧС?
* **Подвижных, стационарных**
* Временных, стационарных
* Оперативных, постоянных
* Передвижных, стационарных

### 19. К каким силам РСЧС относятся формирования государственного санитарно-эпидемиологического надзора РФ?
* Ликвидации ЧС
* **Наблюдения и контроля**
* Предупреждения ЧС

### 20. При помощи какой системы осуществляется информационное обеспечение в РСЧС?
* **АИУС**
* КСЭОН
* ОКСИОН
* РАСЦО
* 112

### 21. Что означает в аббревиатуре "ОКСИОН" первая и вторая буквы?
* Общая контрольная
* Объектная корпоративная
* **Общероссийсая комплексная**
* Оперативная координационная
* Основная комбинированная

### 22. Каким нормативно-правовым документом определяются организация и функционирование РСЧС?
* Приказом МЧС РФ
* **Постановлением правительства РФ**
* Федеральным законом РФ
* Указом Президента РФ

### 23. День спасателя:
* **27.12**
* 04.11
* 04.10
* 01.03

### 24. Из средств какого бюджета финансируется ТП РСЧС?
* Федерального
* Местного
* **Регионального**
* Организацией
* Государственного резервного фонда

### 25. Сколько создано ТП РСЧС в РФ?
* 84
* 87
* **85**
* 86
* 83

### 26. К какому виде органов управления относится РЦ ГОЧС?
* К координационным
* К общественным 
* К постоянно-действующим
* **К координирующим**
* К повседневным

### 27. В какой системе власти создана РСЧС?
* Местной
* Законодательной
* **Исполнительной**
* Судебной

### 28. Что имеет каждый уровень РСЧС для защиты населения и территорий от ЧС?
* **Органы управления, силы и средства, финансовые и материальные ресурсы, системы связи оповещения и информационного оповещения**
* Органы управления, силы и средства, финансовые резервы, государственные учреждения специально уполномоченные решать вопросы защиты от ЧС, материальные резервы

### 29. Каким органом государственной власти определяется состав сил РСЧС?
* МЧС РФ
* Президентом РФ
* Руководителем федерального органа исполнительной власти
* **Правительством РФ**

### 30. РСЧС - это:
* **Единая государственная система предупреждения и ликвидации ЧС**
* Российская система предупреждения и действий
* Российская система предупреждения и ликвидации ЧС
* Государственная система предупреждения и действий в ЧС
* Единая государственная система предупрежения и действий ЧС

### 31. По решению какого органа власти РФ предоставляеся международная гуманитарная помощь?
* **Правительства РФ**
* Парламента
* Президента РФ
* Совета безопасности РФ
* Совета Федерации

### 32. С помощью какой системы осуществляется информационное обеспечение в РСЧС?
* ЕДДС
* **АИУС**
* ЦУКСы
* ДДС
* Телефон 01

### 33. Где создаются территориальные подсистемы РСЧС?
* В регионах РФ
* В министерствах, агенствах, службах
* В муниципальных образованиях
* В административных округах РФ
* **В субъектах РФ**

### 34. К силам и средствам РСЧС относятся (по назначению)
* Аварийно-спасательные формирования
* **Силы и средства наблюдения и контроля, силы ис релства ликвидации ЧС**
* Войска ГО, силы постоянной готовности

### 35. Какой орган исполнительной власти утверждает перечень сил и постоянной готовности РСЧС на федеральном уровне?
* МЧС
* **Правительство РФ**
* КЧС
* Администрация субъектов РФ
* Министерства, где создаются эти силы

### 36. На скольких уровнях РСЧС создаются системы экстренного оповещения населения о ЧС?
> В настоящее время в Российской Федерации созданы и функционируют 
>* региональные, 
>* местные и 
>* локальные (объектовые) системы оповещения населения.

* Одном
* Пяти
* Двух
* Четырех
* **Трех**

### 37. Какой федеральный орган исполнительной власти проводит аттестации аварийно-спасательных служб?
* **МЧС России**
* Правительство РФ
* Министерства, где создаются эти службы
* Совет безопасности РФ

### 38. По решению какого органа власти осуществляется ликвидация трансграничной ЧС?
* Совета Безопасности РФ
* Президента РФ
* **Правительства РФ**
* Главы администрации, где сложилась ЧС

### 39. На каких уровнях РСЧС создаются КЧС и ПБ?
* На всех, за исключением Регионального
* На всех
* На всех, за исключением Федерального
* **На всех, за исключением межрегионального**

### 40. Где создаются функциональные подсистемы РСЧС?
* В административных округах РФ
* **В министерствах, агентствах, службах**
* В субъектах РФ
* В муниципальных образованиях
* В регионах РФ

### 41. Какие органы управления РСЧС являются постоянно действующими на региональном и муниципальном уровнях?
* **ГУ МЧС, подразделения ГОЧС при администрациях**
* Отделы ГОЧС, центры ГОЧС
* КЧС, ГУ ГОЧС

### 42. На каком уровне РСЧС создаются звенья ТП РСЧС?
* Объектовом
* Муниципальном, Объектовом
* Федеральном
* **Региональном** [1]

### 43. Во сколько эшелонов могут применяться силы и средства РСЧС?
* Чертыре
* Не эшелонируются
* **Три**

### 44. Какие органы управления имеет РСЧС?
* Координирующие, постоянно действующие, временные
* Федеральные и органы упавления в субъектах РФ
* Постоянно действующие, территориальных, федеральные
* **Координационные, постоянно действующие, повседневные**
* Федеральные, региональные, территориальные, местные

### 45. Какой установлен звуковой сигнал для экстренной эвакуации людей из зданий университета?
* **Продолжительный звонок**
* Сирена
* Гудок
* Три коротких звонка
* Прерывистый звонок

### 46. Какие координационные органы управления имеются на федеральном уровне РСЧС?
* МЧС, межведомственные комиссии и ведомственные комиссии 
* Правитесльственная комиссия, комиссия по оперативным вопросам, МЧС, межведомственная комиссия
* МЧС, правительсвенная комиссия, ведомственная комиссия
* **Правительственная комиссия по предупреждению и ликвидации ЧС и обеспечению пожарной безопасности**

### 47. К какому виду органов управления РСЧС относятся ЦУКСы?
### 47.1 К каким органам управления РСЧС относятся центры управления в кризисных ситуациях?
* Чрезвычайным
* Координационным
* Постоянно действующим
* Оперативным
* **Повседневным**

### 48. Где создаются звенья РСЧС?
* **В территориальных подсистемах**
* В функциональных подсистемах
* Не создаются
* На региональным уровне

### 49. Какой уровень реагирования на ЧС устанавливается, если привлекаются Вооруженные Силы РФ и другие войска.
* Региональный
* **Особый**
* Федеральный
* Высший
* Специальный

### 50. Решениями каких должностных лиц в субъекте РФ может устанавливаться функционирование РСЧС в режиме повышенной готовности?
* **Глава администрации субъекта РФ**
* Министром МЧС РФ
* Председателем правительства РФ
* Начальником ГУ МЧС по субъекту РФ
* Президентом РФ

### 51. Постояно действующим органом управления на региональном уровне РСЧС
* РЦ МЧС по административному округу
* МЧС
* КЧС и ПБ
* **ГУ МЧС по субъекту РФ**

### 52. Какой единый номер будет установлен в телефонной сети для вызова экстренных служб реагирования в 2017 году?
* 03
* 911
* 01
* **112**
* 02

### 53. К какому федеральному административному округу отностится Омская область?
* Уральскому
* Западно-сибирскому
* Восточно-сибирскому
* **Сибирскому**

### 54. Какой уровень реагирования устанавливается при ликвидации межмуниципальной ЧС?
* Объектовый
* Особый
* Местный
* Федеральный
* **Региональный**

### 55. Сколько уровней имеет ТП РСЧС?
* Пять [2]
* Один
* Два [1]
* Четыре
* Три [1]

### 56. Сколько уровней имеет РСЧС?
* Четыре
* **Пять**
* Один
* Три
* Два

### 57. К каким силам и средствам относятся формирования всероссийской службы медицины катастроф?
* **Ликвидации ЧС**
* Наблюдения и контроля
* Предупреждения ЧС
* ГО

### 58. В каких органах гос. власти создаются подсистемы РСЧС?
* В федеральных органах законодательной власти
* **В федеральных органах исполнительной власти**
* В федеральных органах судебной власти
* В федеральных органах законодательной, судебной, исполнительной властей

### 59. К органам повседневного управления РСЧС относятся:
* РЦ МЧС, ГУ МЧС, ЦУКСы
* **ЦУКСы, ЕДДС, ДДС**
* ЦУКСы, ЕДДС, ДДС, ГУ МЧС, РЦ МЧС
* ЕДДС, ДДС, ПСС
* Территориальные органы управления МЧС

### 60. Какой уровень реагирования устанавливается при ликвидации локальной ЧС?
* Местный
* Межмуниципальный
* Не устанавливается
* Муниципальный
* **Объектовый**

### 61. Какое должностное лицо исполнительной власти субъекта РФ может возглавлять КЧС?
* **Заместитель главы администрации**
* Председатель Законодательного собрания субъекта РФ
* Начальник ГУ ГОЧС

### 62. Какими силами и средствами ликвидируется федеральная ЧС?
* **МЧС России**
* Федеральных органов власти
* Местного самоуправления
* Организаций, попавших в зону ЧС
* Субъектов РФ

### 63. Каким постановлением Правительства РФ утверждено "Положение о РСЧС"?
* **№794 от 30.12.03**
* №893 от 05.10.03
* №1114 от 05.12.95
* №802 от 05.09.02
* №1113 от 05.11.95

### 64. Кто организует взаимодействие силами и средствами, привлеченными для ликвидации ЧС?
* Председатель КЧС
* **Руководитель работ по ликвидации ЧС**
* Повседневные органы управления
* ГУ МЧС
* Глава администрации

### 65. Какой орган управления является постоянно действующим на федеральном уровне?
* Межведомственная комиссия по ЧС
* Правительственная КЧС
* **МЧС России**
* Ведомственная комиссия
* РЦ ГОЧС

### 66. К какому виду органов управления относится Правительственная комиссия по предупреждению и ликвидации чрезвычайных ситуаций и обеспечению пожарной безопасности?
* **Координационным**
* Координирующим
* Оперативного управления
* Постоянно действующим
* Повседневным

### 67. Сколько существут видов ЧС, для ликвидации которых используются силы РСЧС?
* **Шесть**
* Семь
* Три
* Четыре
* Пять

### 68. Сколько региональных центров ГОЧС имеется в РФ?
> На территории России создано шесть РЦ:
>* 1) Северо-Западный
>* 2) Центральный
>* 3) Южный
>* 4) Приволжско-Уральский
>* 5) Сибирский
>* 6) Дальневосточный.

* Восемь
* Семь [2]
* **Шесть**
* Девять
* Пять

### 69. Какие органы управления РСЧС являются постоянно действующими на федеральном и межрегиональном уровнях?
* **МЧС, РЦ ГОЧС**
* РЦ ГОЧС, ГУ ГОЧС
* МЧС, РЦ ГОЧС, КЧС
* МЧС, ГО ГОЧС
* МЧС, КЧС

### 70. Какой орган управления является координационным на федеральном уровне РСЧС?
* Ведомственная комиссия
* МЧС
* ГУ ГОЧС
* **Комиссия по ЧС**
* Полномочный представитель Президента

### 71. Какими силами и средствами ликвидируется локальная ЧС?
* Аварийно-спасательными службами
* Органами местного самоуправления
* **Организацией**
* МЧС России

### 72. Какие координационные органы управления имеются на межрегиональном уровне РСЧС?
* РЦ ГОЧС
* **Полномочный представитель Президента** 
* Ведомственная комиссия, региональный центр по делам ГОЧС, комиссия, состоящая из представителей субъектов РФ
* ГУ ГОЧС
* Комиссия по чрезвычайным ситуациям (КЧС)

### 73. Какой орган управления является координационным на региональном уровне РСЧС?
* ГУ МЧС по субъекту РФ
* Ведомственная комиссия
* РЦ ГОЧС
* МЧС
* **КЧС и ПБ**
 
### 74. На каких уровнях РСЧС создаются территориальные подсистемы?
* **На региональном** [1]

### 75. Финансирование объектного уровня РСЧС осуществляется из:
* Бюджета субъекта РФ
* Бюджета муниципальных образований
* Внебюджетных средств
* Федерального бюджета
* **Средств организации**
